#!/bin/bash

function cleanup {
   echo -e "\`==='==='==='==='==='==='==='==='==='==='==='==='==='==='==='===´"
   echo "Bye. Oink!"
}

function stop {
	echo ""
}

trap stop SIGINT SIGTERM
trap cleanup EXIT

echo '                     ________________________
        |\_,,____   /                        \
        ( o__o \/  ( I can do this too! oink! )
        / (..) \  / \ _______________________/
       (_ )--( _)
       / ""--"" \
,===,=| |-,,,,-| |==,===,===,===,===,===,===,===,===,===,===,===,
|   |  WW   |  WW   |   |   |   |   |   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |'
ping "$@" | while read line
do
   echo "| $line"
done
